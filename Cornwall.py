﻿## CURRENTLY SUPPORTS PYTHON 3+ ONLY
# Some teething issues with 2.7...

import os, sys, codecs

EXTENSION_STRING = '.encrypt'
LOWERCASE_ORD_START = ord('a') - 1
UPPERCASE_ORD_START = ord('A') - 1

def GetFile(filepath):        
    if os.path.isfile(os.path(filepath + EXTENSION_STRING)):
        return open(filepath + EXTENSION_STRING, 'rb')
    else:
        return open(filepath + EXTENSION_STRING, wb)

def WriteToFile(filepath, data):
    filepath = filepath + EXTENSION_STRING
    if os.path.isfile(filepath):
        print('WARNING! File', filepath ,'will be overwritten and data may be lost...')
        conf = input('Continue writing to disk? (Y\n)')
        if conf != 'Y':
            return

    file = codecs.open(filepath, 'w', 'utf-8')
    if file:
        file.write(data)
        file.close()
    
def ReadFromFile(filepath):
    filepath = filepath + EXTENSION_STRING
    print(filepath)
    if os.path.isfile(filepath):
        file = codecs.open(filepath, 'r', 'utf-8')
        if file:
            data = file.read()
            file.close()
            return data
    else:
        print('Could not find file', filepath, ', please check the path exists!')
        return ''

def Encrypt(string, keystring):
    keylength = len(keystring)
    encryptstring = ""
    if keylength > 0:
        for pos, letter in enumerate(string):
            ##print(pos, letter)
            encryptstring += chr((ord(letter)) + ToAlphabetOffset(keystring[pos]))

    else:
        print("No key provided!")
    return encryptstring

def Decrypt(encryptstring, keystring):
    keylength = len(keystring)
    string = ""
    keystring = PadKey(keystring, len(encryptstring))
    if keylength > 0:
        for pos, letter in enumerate(encryptstring):
            char = chr((ord(letter)) - ToAlphabetOffset(keystring[pos]))
            string += char
    else:
        print("No key provided!")

    return string

def Garble(string, keystring):
    keylength = len(keystring)
    garblestring = string
    if keylength > 0:
        for pos, letter in enumerate(keystring):
            garblestring = ShiftStringLeft(garblestring, ToAlphabetOffset(letter))

    else:
        print("No key provided!")
    return garblestring

def UnGarble(garblestring, keystring):
    keylength = len(keystring)
    string = garblestring
    if keylength > 0:
        for pos, letter in enumerate(keystring[::-1]):
            string = ShiftStringRight(string, ToAlphabetOffset(letter))

    else:
        print("No key provided!")
    return string

def SwapChars(string, keypos, keychar):
    if keypos > len(string):
        return string

    swappos = ToAlphabetOffset(keychar)

    if swappos > len(string):
        return string
    else:
        ch_l = list(string)
        ch_l[swappos], ch_l[keypos] = ch_l[keypos], ch_l[swappos]
        return ''.join(ch_l)

def ShiftStringLeft(string, offset):
    return string[offset%len(string):] + string[:offset%len(string)]

def ShiftStringRight(string, offset):
    return ShiftStringLeft(string, len(string)-offset)

def ToAlphabetOffset(char):
    if char.isdigit():
        return int(char)
    elif char.isupper():
        return ord(char) - UPPERCASE_ORD_START
    elif char.islower():
        return ord(char) - LOWERCASE_ORD_START
    else:
        return 0

def PadKey(keystring, lengthtopad):
    if len(keystring) >= lengthtopad:
        return keystring[:lengthtopad]
    padkey = ''
    for c in range(lengthtopad):
        padkey += keystring[c % len(keystring)]

    return padkey

## MAIN LOOP STARTS HERE
run = 1

print('Welcome to Cornwall encryption program...')
while(run):
    choice = input('Please enter "d" to decrypt a file or "e" to encrypt text, "q" to quit:\n')

    if choice == 'd':
        file = input('Enter absolute filepath to decrypt (omit ".encrypt" extension):\n')

        encrypted = ReadFromFile(file)
        print(repr(encrypted))

        key = PadKey(input("Enter your key: "), len(encrypted))

        attempt = Decrypt(encrypted, key )
        ungarbletext = UnGarble(attempt, key)

        print('Decrypted as:', repr(ungarbletext))

    elif choice == 'e':
        file = input('Enter absolute filepath to decrypt (omit ".encrypt" extension):\n')
        plaintext = input('Enter your text to encrypt:\n')
        key = input('Enter a key:\n')

        key = PadKey(key, len(plaintext))
        garbletext = Garble(plaintext, key)
        encrypted = Encrypt(garbletext, key)

        print('Text after garbling and encryption is:', repr(encrypted))

        WriteToFile(file, encrypted)

    elif choice == 'q':
        print('Exiting...')
        run = 0
    else:
        print('Input not recognised:', choice)